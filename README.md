# OpenML dataset: Higgs

https://www.openml.org/d/44092

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "regression on numerical features" benchmark. Original description: 
 
This is a smaller version of the original dataset, containing 1M rows. 
**Author**: Daniel Whiteson, University of California Irvine  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/HIGGS)  
**Please cite**: Baldi, P., P. Sadowski, and D. Whiteson. Searching for Exotic Particles in High-energy Physics with Deep Learning. Nature Communications 5 (July 2, 2014).  

**Higgs Boson detection data**. The data has been produced using Monte Carlo simulations. The first 21 features (columns 2-22) are kinematic properties measured by the particle detectors in the accelerator. The last seven features are functions of the first 21 features; these are high-level features derived by physicists to help discriminate between the two classes. There is an interest in using deep learning methods to obviate the need for physicists to manually develop such features. The last 500,000 examples are used as a test set.

**Note: This is the UCI Higgs dataset, same as version 1, but it fixes the definition of the class attribute, which is categorical, not numeric.**


### Attribute Information
* The first column is the class label (1 for signal, 0 for background)
* 21 low-level features (kinematic properties): lepton pT, lepton eta, lepton phi, missing energy magnitude, missing energy phi, jet 1 pt, jet 1 eta, jet 1 phi, jet 1 b-tag, jet 2 pt, jet 2 eta, jet 2 phi, jet 2 b-tag, jet 3 pt, jet 3 eta, jet 3 phi, jet 3 b-tag, jet 4 pt, jet 4 eta, jet 4 phi, jet 4 b-tag
* 7 high-level features derived by physicists: m_jj, m_jjj, m_lv, m_jlv, m_bb, m_wbb, m_wwbb. 

For more detailed information about each feature see the original paper.

Relevant Papers:

Baldi, P., P. Sadowski, and D. Whiteson. Searching for Exotic Particles in High-energy Physics with Deep Learning. Nature Communications 5 (July 2, 2014).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44092) of an [OpenML dataset](https://www.openml.org/d/44092). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44092/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44092/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44092/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

